<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_6844 Automatic Tests</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>211b3d5e-802e-48a3-86c8-0d1560374176</testSuiteGuid>
   <testCaseLink>
      <guid>1d818d98-e121-4d57-bb9b-64f979f7f0df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CURA Healthcare Service/TC_6844 Login Test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70156803-25ac-47e0-90fd-8b5d6165cfce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CURA Healthcare Service/TC_6844 Make Appointment</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
