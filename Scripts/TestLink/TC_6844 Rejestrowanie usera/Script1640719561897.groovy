import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(findTestData('TestLink').getValue('url', 1))

WebUI.click(findTestObject('Object Repository/Page_Sklep internetowy demo/b_KONTO'))

WebUI.click(findTestObject('Page_Logowanie/a_Za konto'))

WebUI.setText(findTestObject('Page_Rejestracja/input_Rejestracja_email'), findTestData('TestLink').getValue('email', 1))

WebUI.setEncryptedText(findTestObject('Page_Rejestracja/input_Adres e-mail_gsfPass1'), findTestData('TestLink').getValue(
        'password', 1))

WebUI.setEncryptedText(findTestObject('Page_Rejestracja/input_Haso_gsfPass2'), findTestData('TestLink').getValue('password', 
        1))

WebUI.click(findTestObject('Page_Rejestracja/label_Przykadowa klauzula akceptacji regulaminu'))

WebUI.click(findTestObject('Page_Rejestracja/button_Rejestruj'))

WebUI.closeBrowser()

